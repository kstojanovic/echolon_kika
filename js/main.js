$(document).ready(function () {
    $(".common_questions-header").click(function () {
        $(this).toggleClass('common_questions-header-active');
        if ($(this).attr('class').indexOf('open'))
            $(this).toggleClass("open").next().slideToggle();
        //Hide the other panels
        $(".common_questions-containercontent").not($(this)).removeClass("open");
        $(".common_questions-header").not($(this)).removeClass("common_questions-header-active");
        $(".common_questions-content").not($(this).next()).slideUp('fast');
    });

    $(".main_navigation-item_link").click(function () {
        $(".main_navigation-item_link").removeClass("active");
        $(this).addClass("active");
    });

    // Popup box

    $(".box-content_popup_button").click(function () {
        $(".box-popup_container").show();
        $(".box-container_popup_background").show();
    });
    $(".box-popup_close_button").click(function () {
        $(".box-popup_container").hide();
        $(".box-container_popup_background").hide();
    });

    // Mobile navigation

    $('.header-menu_toggle').click(function() {
        $('.mobile_header-main_navigation').slideToggle();
    });

    $('.box_list-button').click(function() {
        $(this).closest(".box_list-item").find(".box_list-paragraph").css( "height", "auto" );
    });

    $('.one-time').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $('.prev'),
        nextArrow: $('.next')
    });

    $('.testimonials-slider').slick({
        centerMode: true,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1500,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        responsive: [
            {
                settings: {
                    arrows: true,
                    centerMode: true,
                    slidesToShow: 1
                }
            },
            {
                settings: {
                    arrows: true,
                    centerMode: true,
                    slidesToShow: 1
                }
            }
        ]
    });

    // Tabs container

    const tabs = document.querySelectorAll('[data-tab-target]')
    const tabContents = document.querySelectorAll('[data-tab-content]')

    tabs.forEach(tab => {
        tab.addEventListener('click', ()  => {
            const target = document.querySelector(tab.dataset.tabTarget)
            tabContents.forEach(tabContent => {
                tabContent.classList.remove('active')
            })
            tabs.forEach(tab => {
                tab.classList.remove('active')
            })
            tab.classList.add('active')
            target.classList.add('active')
        })
    })
});

